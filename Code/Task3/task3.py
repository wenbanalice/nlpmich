import nltk
import csv
import gensim
import numpy

#nltk.download('wordnet')
from nltk.stem.porter import *
from nltk.corpus import wordnet as wn

#model = gensim.models.KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin', binary=True)

def readincache(name):
    d={}
    cfp= open(name, 'r')
    rows= cfp.readlines()
    for row in rows:
        row.split(',')
        d[row[0]]=row[1]
    return d

def writeincache(name,d):
    cfp= open(name, 'w')
    for word in d.keys():
       cfp.write(str(word+","+str(d[word])+"\n"))
    cfp.close

def wordnet(word1,t,word2):
    print(word2)
    stemmer = PorterStemmer()
    word1s = wn.synsets(stemmer.stem(word1))
   # print(word1s)
    word2s = wn.synsets(stemmer.stem(word2))
    n=0

    words1p=[]

    for w1 in word1s:
       if w1.pos()==t:
          for l in w1.lemmas():
                words1p = words1p + wn.synsets(l.name())


    for w1 in word1s:
        for w2 in word2s:
            m= wn.wup_similarity(w1,w2)
            if not(m==None):
                n=max(n,m)
    return n

def w2vsim(word1,word2):
    global model
    a = model.wv[word1]
    b = model.wv[word2]

    dot = np.dot(a, b)
    norma = np.linalg.norm(a)
    normb = np.linalg.norm(b)
    cos = dot / (norma * normb)
    
    return cos

text = "text1"
masterword= "drip"
mastert="v"

fp = open(text,"r")
lines = fp.readlines()
fp.close()
maxsim=[]

#wncache = readincache("wn.csv")
  
wordsim=[]
for line in lines:
    if not(line == '\n'):
        m=0
        w=""
        for word in line.split(" "):
            n = wordnet(masterword,mastert,word)
            wordsim=wordsim+[(n,word)]
            if(n>m):
                w=word
                m=n
        maxsim=maxsim+[(m,line,w)]

maxsim.sort(key=lambda tup: -tup[0])
wordsim.sort(key=lambda tup: -tup[0])

fp = open("log", "w")
for m in maxsim:
    fp.write(str(m)+"\n")
fp.close

fp = open("log2", "w")
for m in wordsim:
    fp.write(str(m)+"\n")
fp.close

#writeincache("wn.csv",wncache)

print(wordnet(masterword,"v","droplets"))