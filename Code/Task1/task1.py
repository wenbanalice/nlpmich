import glob
import math
import subprocess
import operator
import scipy
from nltk.stem.porter import *

def makeStemSave():
	(posFiles,negFiles)=readin()
	fileheadneg = "../../STEMNEG/"
	fileheadpos = "../../STEMPOS/"
	stemmer = PorterStemmer()

	for pos in posFiles:
		sf = open(fileheadpos+pos[10:],"w+")
		f=open(pos,"r")
		words=f.readlines()
		for word in words:
			words = word.strip()
			stemed = stemmer.stem(word)
			sf.write(stemed+"\n")
		f.close()
		sf.close()

	for neg in negFiles:
		sf = open(fileheadneg+neg[10:],"w+")
		f=open(neg,"r")
		words=f.readlines()
		for word in words:
			words = words.strip()
			stemed = stemmer.stem(word)
			sf.write(stemed+"\n")
		f.close()
		sf.close()

def readin():
	print("Stemming not in use")
	negFiles = glob.glob('../../NEG/*')
	posFiles = glob.glob('../../POS/*')
	return(posFiles,negFiles)
	
def readinStemmed():
	print("Stemming in use")
	negFiles = glob.glob('../../STEMNEG/*')
	posFiles = glob.glob('../../STEMPOS/*')
	return(posFiles,negFiles)

def roundRobinFolding(files,n):
	count = 0
	folds=[]
	for file in files:
		if len(folds)>count:
			folds[count].append(file)
		else:
			folds.append([file])
		count=count+1
		count=count%n
	return folds

def tokenCountSmoothed(files,useBigram=True):
	Tokens={}
	tokenCount={"pos":0 , "neg":0}
	fileCount={"pos":0 , "neg":0}

	for (file,label) in files:
		fileCount[label]=fileCount[label]+1
		f= open(file,"r")
		words=f.readlines()
		for i in range(1,len(words)):
			word=words[i]
			if word not in Tokens:
				Tokens[word]={"pos":1 , "neg":1}
				tokenCount["pos"] = tokenCount["pos"]+1
				tokenCount["neg"] = tokenCount["neg"]+1
			Tokens[word][label] = Tokens[word][label]+1
			tokenCount[label]=tokenCount[label]+1

			if(i>0 and useBigram):
				bigram = words[i-1]+" "+words[i]
				if bigram not in Tokens:
					Tokens[bigram]={"pos":1 , "neg":1}
					tokenCount["pos"] = tokenCount["pos"]+1
					tokenCount["neg"] = tokenCount["neg"]+1
				Tokens[bigram][label] = Tokens[bigram][label]+1
				tokenCount[label]=tokenCount[label]+1

		f.close()
	return (Tokens, tokenCount,fileCount)

def logProbs(Tokens, TokenCount):
	logProbTokens={}
	for token in Tokens:
		logProbTokens[token] = {}
		logProbTokens[token]["pos"]= math.log(Tokens[token]["pos"]/TokenCount["pos"])
		logProbTokens[token]["neg"] = math.log(Tokens[token]["neg"]/TokenCount["neg"])
	return logProbTokens

def label(lab,files):
	labledFiles=[]
	for file in files:
		labledFiles.append((file,lab))
	return labledFiles

def NB(training,classify,frequency=True,useBigram=True):
	results={}
	(Tokens, TokenCount ,FileCount) = tokenCountSmoothed(training,useBigram)
	logProbTokens = logProbs(Tokens,TokenCount)

	for (file,_) in classify:
		posCount =math.log(FileCount["pos"]/FileCount["pos"]+FileCount["neg"])
		negCount =math.log(FileCount["neg"]/FileCount["pos"]+FileCount["neg"])
		dictWords=set()
		f= open(file,"r")
		words=f.readlines()
		for i in range(0,len(words)):
			word=words[i]
			if word in logProbTokens:
				if(frequency or word not in dictWords):
					posCount=posCount+logProbTokens[word]["pos"]
					negCount=negCount+logProbTokens[word]["neg"]				
			else:
				if(frequency or word not in dictWords):
					posCount=posCount+math.log(1/TokenCount["pos"])
					negCount = negCount+math.log(1/TokenCount["neg"])
			dictWords.add(word)
			if(i>0 and useBigram):
				bigram=words[i-1]+" "+words[i]
				if bigram in logProbTokens:
					if(frequency or bigram not in dictWords):
						posCount=posCount+logProbTokens[bigram]["pos"]
						negCount=negCount+logProbTokens[bigram]["neg"]				
				else:
					if(frequency or bigram not in dictWords):
						posCount=posCount+math.log(1/TokenCount["pos"])
						negCount = negCount+math.log(1/TokenCount["neg"])
		if posCount>negCount:
			results[file] = "pos"
		else:
			results[file]="neg"
		f.close()
	return results

def NBNFold(n,posFiles,negFiles,frequency=True,useBigram=True):
	posfolds =roundRobinFolding(label("pos",posFiles),n)
	negfolds = roundRobinFolding(label("neg",negFiles),n)
	
	totalResults=[]
	resultsList=[]
	for i in range(0,n):
		resultsList.append([])
		training=[]
		for j in range(0,n):
			if i==j:
				testdata= posfolds[j]+negfolds[j]
			else:
				training = training +posfolds[j]+negfolds[j]
		results = NB(training,testdata,frequency,useBigram)
		correct=0
		total=0
		for (file, lab) in testdata:
			if results[file] ==lab:
				correct=correct+1
				resultsList[i].append(1)
			else:
				resultsList[i].append(0)
			total=total+1
		totalResults.append(correct/total)
	t=0
	for i in range(0,n):
		t=t+totalResults[i]
	print("NB :" + str(totalResults) +" : " +str(t/n))
	return(t/n,resultsList)

def transformToSVM(globalDict,labeledFile,frequency,useBigram):
	(file,label) = labeledFile
	num = 1 if label=="pos"else -1		
	fileFeaturesdict={}
	f= open(file,"r")
	words=f.readlines()
	for i in range(0,len(words)):
		word=words[i]
		if word in globalDict:
			featureCode = globalDict[word]
		else:
			featureCode=len(globalDict)+1
			globalDict[word]=featureCode
		if featureCode in fileFeaturesdict and frequency:
			featureCode = globalDict[word]
			fileFeaturesdict[featureCode] = fileFeaturesdict[featureCode]+1
		else:	
			fileFeaturesdict[featureCode]=1

		if(i>0 and useBigram):
			bigram = words[i-1]+ " "+ words[i]
			if bigram in globalDict:
				featureCode = globalDict[bigram]
			else:
				featureCode=len(globalDict)+1
				globalDict[bigram]=featureCode
			if featureCode in fileFeaturesdict and frequency:
				featureCode = globalDict[bigram]
				fileFeaturesdict[featureCode] = fileFeaturesdict[featureCode]+1
			else:	
				fileFeaturesdict[featureCode]=1
	f.close()
	featureList = []
	for tokens in fileFeaturesdict:
		featureList.append((tokens, fileFeaturesdict[tokens]))
	return (num,featureList)


def dataToFile(data,string):
	f = open(string,"w+")
	for (label,features) in data:
		features.sort(key = operator.itemgetter(0))
		text = str(label)
		for (feature,value) in features:
			text= text+" "+ str(feature)+":"+str(value)
		f.write(text+"\n")
	f.close()
	
def SVM(training,testing,frequency=True,useBigram=True):
	trainingdata=[]
	testdata=[]
	globalDict={}
	for train in training:
		if(frequency):
			trainingdata.append(transformToSVM(globalDict,train,frequency,useBigram))
		else:
			trainingdata.append(transformToSVM(globalDict,train,frequency,useBigram))
#	print("transform compelted")
	for test in testing:
		testdata.append(transformToSVM(globalDict,test,frequency,useBigram))
#	print("transform compelted")

	dataToFile(trainingdata,"training")
	#print(trainingdata)
	subprocess.run(["../SVM/svm_learn","-v","0","./training", "./model"],True)
#	print("model done")
	dataToFile(testdata,"testing")
	subprocess.run(["../SVM/svm_classify","-v","0","./testing", "./model","./prediction"],True)
#	print("prediction done")
	
	return "./prediction"

def SVMNFold(n,posFiles,negFiles,frequency=True,useBigram=True):
	posfolds =roundRobinFolding(label("pos",posFiles),n)
	negfolds = roundRobinFolding(label("neg",negFiles),n)
	
	totalResults=[]
	resultsList=[]
	for i in range(0,n):
		resultsList.append([])
		training=[]
		for j in range(0,n):
			if i==j:
				testdata= posfolds[j]+negfolds[j]
			else:
				training = training +posfolds[j]+negfolds[j]
		results=[]
		prediction = SVM(training,testdata,frequency,useBigram)
		fp= open(prediction,"r")
		results=fp.readlines()
		correct=0
		total=0
		for (file, lab) in testdata:
			val= float(results[total].rstrip("\n"))
			if (val>0 and lab=='pos') or (val<0 and lab=='neg'):
				resultsList[i].append(1)
				correct=correct+1
			else:
				resultsList[i].append(0)
			total=total+1
		totalResults.append(correct/total)
		fp.close()
	t=0
	for i in range(0,n):
		t=t+totalResults[i]
	print("SVM :" + str(totalResults) + " : "+ str(t/n))
	return(t/n,resultsList)	

def signTest(systemA,systemB):
	plus=0
	minus=0
	null=0

	A=[]
	B=[]
	for index in range(0,len(systemA)):
		A=A+systemA[index]
		B=B+systemB[index]
	for index in range(0,len(A)):
		if(A[index]>B[index]):
			plus=plus+1
		elif(A[index]<B[index]):
			minus=minus+1
		else:
			null=null+1

	N= 2 *math.ceil(null/2) +plus +minus
	k= math.ceil(null/2) + min(plus,minus)

	p=scipy.stats.binom_test(k,N)
	return p

def psig(p):
	if(p<0.01):
		print("significnt to 0.01")
	elif(p<0.025):
		print("significant t0 0.025")
	else:
		print("not significant")

#makeStemSave()


(posFiles,negFiles) =readinStemmed()
n=10

print("UNIGRAM")
print(str(n)+ " fold test for NB VS SVM Frequency")
(accuracyNB,ResultsNB) = NBNFold(n,posFiles,negFiles,True,False)
(accuracySVM,ResultsSVM) = SVMNFold(n,posFiles,negFiles,True,False)

p=signTest(ResultsNB,ResultsSVM)
print("SVM vs NB signtest p: "+str(p))
psig(p)

print(str(n)+ " fold test for NB VS SVM Presence")
(accuracyNB,ResultsPNB) = NBNFold(n,posFiles,negFiles,False,False)
(accuracySVM,ResultsPSVM) = SVMNFold(n,posFiles,negFiles,False,False)

p=signTest(ResultsPNB,ResultsPSVM)
print("Presence SVM vs NB signtest p: "+str(p))
psig(p)

p=signTest(ResultsNB,ResultsPNB)
print("NB vs precence NB signtest p: "+str(p))
psig(p)

p=signTest(ResultsSVM,ResultsPSVM)
print("SVM vs precence SVM signtest p: "+str(p))
psig(p)

print("BIGRAM")
print(str(n)+ " fold test for NB VS SVM Frequency")
(accuracyNB,ResultsNB) = NBNFold(n,posFiles,negFiles,True,True)
(accuracySVM,ResultsSVM) = SVMNFold(n,posFiles,negFiles,True,True)

p=signTest(ResultsNB,ResultsSVM)
print("SVM vs NB signtest p: "+str(p))
psig(p)

print(str(n)+ " fold test for NB VS SVM Presence")
(accuracyNB,ResultsPNB) = NBNFold(n,posFiles,negFiles,False,True)
(accuracySVM,ResultsPSVM) = SVMNFold(n,posFiles,negFiles,False,True)

p=signTest(ResultsPNB,ResultsPSVM)
print("Presence SVM vs NB signtest p: "+str(p))
psig(p)

p=signTest(ResultsNB,ResultsPNB)
print("NB vs precence NB signtest p: "+str(p))
psig(p)

p=signTest(ResultsSVM,ResultsPSVM)
print("SVM vs precence SVM signtest p: "+str(p))
psig(p)