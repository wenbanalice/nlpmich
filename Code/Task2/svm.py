import glob
import math
import subprocess
import operator
import scipy
import task2
import numpy as np
from gensim.models import Word2Vec
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from gensim.utils import simple_preprocess
import csv

def dataToFile(data,string):
	f = open(string,"w+")
	for (label,features) in data:
		features.sort(key = operator.itemgetter(0))
		text = str(label)
		for (feature,value) in features:
			text= text+" "+ str(feature)+":"+str(value)
		f.write(text+"\n")
	f.close()

def dataToString(data,acc):
    string=""
    for param in data.keys():
        string = string + str(data[param][0]) + "," +str(data[param][1])+","
    string = string + str(acc)
    return string

def readinVals(log):
    f=open(log,"r")
    read= csv.reader(f, delimiter=',')
    readCSV=list(read)
    rows = len(readCSV)-1  

    tests = {"vs":[int(readCSV[rows][0]),int(readCSV[rows][1])], 
    "wind":[int(readCSV[rows][2]),int(readCSV[rows][3])],
    "neg":[int(readCSV[rows][4]),int(readCSV[rows][5])] ,
    "min":[int(readCSV[rows][6]),int(readCSV[rows][7])], 
    "ep":[int(readCSV[rows][8]),int(readCSV[rows][9])]}
    baseline = float(readCSV[rows][10])

    f.close()
    return (tests,baseline)
    
def BOWSVM(globalDict,labeledFile):
    (file,label) = labeledFile
    num = 1 if label=="pos"else -1		
    fileFeaturesdict={}
    f= open(file,"r")
    words=f.read()
    words= simple_preprocess(words)
    for i in range(0,len(words)):
        word=words[i]
        if word in globalDict:
            featureCode = globalDict[word]
        else:
            featureCode=len(globalDict)+1
            globalDict[word]=featureCode
        fileFeaturesdict[featureCode]=1

        if(i>0):
            bigram = words[i-1]+ " "+ words[i]
            if bigram in globalDict:
                featureCode = globalDict[bigram]
            else:
                featureCode=len(globalDict)+1
                globalDict[bigram]=featureCode
            fileFeaturesdict[featureCode]=1
    f.close()
    featureList = []
    for tokens in fileFeaturesdict:
        featureList.append((tokens, fileFeaturesdict[tokens]))
    return (num,featureList)




def SVMform(model,labeledFile):
    (file,label) = labeledFile
    num = 1 if label=="pos"else -1	
    f=open(file,"r")
    words=f.read()
    cwords= simple_preprocess(words)
    model.random.seed(0)
    vec =model.infer_vector(cwords, steps=model.iter)
    featureVec=[]
    i=1
    for v in vec:
        featureVec.append((i,v))
        i=i+1
    return(num,featureVec)

def SVM(training,testing,modelFile,BOW):
    model = Doc2Vec.load(modelFile)
    trainingdata=[]
    testdata=[]
    globalDict={}
    for train in training:
        if not BOW:
            trainingdata.append(SVMform(model,train))
        else:
            trainingdata.append(BOWSVM(globalDict,train))

    for test in testing:
        if not BOW:
            testdata.append(SVMform(model,test))
        else:
            testdata.append(BOWSVM(globalDict,test))

    dataToFile(trainingdata,"training")
    subprocess.run(["../SVM/svm_learn","-v","0","./training", "./model"],True)
    dataToFile(testdata,"testing")
    subprocess.run(["../SVM/svm_classify","-v","0","./testing", "./model","./prediction"],True)

    return "./prediction"

def label(lab,files):
	labledFiles=[]
	for file in files:
		labledFiles.append((file,lab))
	return labledFiles

def readin():
    negFiles = glob.glob('../../NEG/*')
    posFiles = glob.glob('../../POS/*')
    return(posFiles,negFiles)

def roundRobinFolding(files,n):
	count = 0
	folds=[]
	for file in files:
		if len(folds)>count:
			folds[count].append(file)
		else:
			folds.append([file])
		count=count+1
		count=count%n
	return folds

def paramTuning():
    (posFiles,negFiles)=readin()
    n=10
    posfolds =roundRobinFolding(label("pos",posFiles),n)
    negfolds = roundRobinFolding(label("neg",negFiles),n)
    validation=[]
    training=[]
    for j in range(0,n):
        if j==0:
            validation= posfolds[j]+negfolds[j]
        else:
            training = training +posfolds[j]+negfolds[j]

    (tests,baseline)=readinVals("log")
    logf= open("log","a+")
    inf= dataToString(tests,baseline)
    print("first: "+str(tests))
    logf.close()
    for n in [5,1,-1]:
        for param in tests.keys():
            while (not bool(int(tests[param][1]))):
                name = "test1"
                tests[param][0] = int(tests[param][0])+n
                
                print("starting :"+str(tests)+"\n")
                task2.loadAndSaveDocToVed(name, tests["vs"][0],tests["wind"][0],tests["neg"][0],tests["min"][0],tests["ep"][0])
                print("finished dtv")
                prediction = SVM(training,validation,name)
                print("finished predictions")
                #test
                fp= open(prediction,"r")
                results=fp.readlines()
                correct=0
                total=0
                resultsList=[]
                for (file, lab) in validation:
                    val= float(results[total].rstrip("\n"))
                    if (val>0 and lab=='pos') or (val<0 and lab=='neg'):
                        resultsList.append(1)
                        correct=correct+1
                    else:
                        resultsList.append(0)
                    total=total+1
                fp.close()

                acc = correct/total
                print("restult: "+ str(acc))

                if acc< baseline:
                    tests[param][0] = tests[param][0]-n
                    tests[param][1] = 1
                else:
                    baseline=acc

                print("now: "+ str(tests)+"\n")
                logf=open("log","a")
                logf.write(dataToString(tests,baseline)+"\n")
                logf.close()
        for param in tests.keys():
            tests[param][1]=0


    print("done")

def validationtest(posFiles,negFiles,n):
    posfolds =roundRobinFolding(label("pos",posFiles),n)
    negfolds = roundRobinFolding(label("neg",negFiles),n)
    validation=[]
    training=[]
    for j in range(0,n):
        if j==0:
            validation= posfolds[j]+negfolds[j]
        else:
            training = training +posfolds[j]+negfolds[j]

    prediction = SVM(training,validation,name)

    fp= open(prediction,"r")
    results=fp.readlines()
    correct=0
    total=0
    resultsList=[]

    for (file, lab) in validation:
        val= float(results[total].rstrip("\n"))
        if (val>0 and lab=='pos') or (val<0 and lab=='neg'):
            resultsList.append(1)
            correct=correct+1
        else:
            resultsList.append(0)
        total=total+1
    fp.close()

    acc = correct/total
    fp=open("log2","w")
    fp.write(str(acc))
    fp.close()
    print("restult: "+ str(acc))


def finalTest(name,posFiles,negFiles,TposFiles,TnegFiles,n,BOW=False):
    posfolds =roundRobinFolding(label("pos",posFiles),n)
    negfolds = roundRobinFolding(label("neg",negFiles),n)
    Tposfolds =roundRobinFolding(label("pos",TposFiles),n)
    Tnegfolds = roundRobinFolding(label("neg",TnegFiles),n)
    Final=[]
    TFinal=[]
    for j in range(0,n):
        if not(j==0):
            Final = Final +posfolds[j]+negfolds[j]
            TFinal = TFinal +Tposfolds[j]+Tnegfolds[j]
    folds = roundRobinFolding(Final,10)
    Tfolds= roundRobinFolding(TFinal,10)
    acc=[]
    totalResults=[]
    for i in range(0,n):
        test=[]
        training=[]
        for j in range(0,n):
            if j==i:
                test= Tfolds[j]
            else:
                training = training +folds[j]
        prediction = SVM(training,test,name,BOW)
        fp= open(prediction,"r")

        results=fp.readlines()
        correct=0
        total=0
        resultsList=[]

        for (_, lab) in test:
            val= float(results[total].rstrip("\n"))
            if (val>0 and lab=='pos') or (val<0 and lab=='neg'):
                resultsList.append(1)
                correct=correct+1
            else:
                resultsList.append(0)
            total=total+1
        fp.close()

        accuracy = correct/total
        fp=open("log2","w")
        fp.write(str(acc))
        fp.close()
       # print("restult: "+ str(accuracy))
        acc=acc+[accuracy]
        totalResults=totalResults+resultsList
    a=np.array(acc)
    return(a.mean(),totalResults)
  

def CompbinedfinalTest(name,posFiles,negFiles,TposFiles,TnegFiles,n,BOW=False):
    posfolds =roundRobinFolding(label("pos",posFiles),n)
    negfolds = roundRobinFolding(label("neg",negFiles),n)
    Tposfolds =roundRobinFolding(label("pos",TposFiles),n)
    Tnegfolds = roundRobinFolding(label("neg",TnegFiles),n)
    Final=[]
    TFinal=[]
    for j in range(0,n):
        if not(j==0):
            Final = Final +posfolds[j]+negfolds[j]
            TFinal = TFinal +Tposfolds[j]+Tnegfolds[j]
    folds = roundRobinFolding(Final,10)
    Tfolds= roundRobinFolding(TFinal,10)
    acc=[]
    totalResults=[]
    for i in range(0,n):
        test=[]
        training=[]
        for j in range(0,n):
            if j==i:
                test= folds[j]
            else:
                training = training +folds[j] + Tfolds[j]
        prediction = SVM(training,test,name,BOW)
        fp= open(prediction,"r")

        results=fp.readlines()
        correct=0
        total=0
        resultsList=[]

        for (_, lab) in test:
            val= float(results[total].rstrip("\n"))
            if (val>0 and lab=='pos') or (val<0 and lab=='neg'):
                resultsList.append(1)
                correct=correct+1
            else:
                resultsList.append(0)
            total=total+1
        fp.close()

        accuracy = correct/total
        fp=open("log2","w")
        fp.write(str(acc))
        fp.close()
       # print("restult: "+ str(accuracy))
        acc=acc+[accuracy]
        totalResults=totalResults+resultsList
    a=np.array(acc)
    return(a.mean(),totalResults)

#name = "cpo"
#task2.loadAndSaveDocToVed(name, 110,15,0,2,5)
#(posFiles,negFiles)=readin()
#n=10


#validationtest(posFiles,negFiles,n)
#(av,results)=finalTest(name,posFiles,negFiles,10)