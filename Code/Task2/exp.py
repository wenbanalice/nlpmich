import glob
import math
import subprocess
import operator
import scipy
import task2
import nltk
import pandas as pd
import numpy as np
from datamuse import datamuse
from gensim.models import Word2Vec
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from gensim.utils import simple_preprocess
import random
import task2
import svm

anto_cache=pd.DataFrame()
synanto_cache=pd.DataFrame()
datamuse_api = datamuse.Datamuse()

def readin():
    negFiles = glob.glob('../../NEG/*')
    posFiles = glob.glob('../../POS/*')
    posnegFiles = glob.glob('PosifedNeg/*')
    negposFiles = glob.glob('NegifedPos/*')
    return(posFiles,negFiles,posnegFiles,negposFiles)

#label word()
def label(doc):
    tagged= nltk.pos_tag(doc)
    return tagged

#get and chache antynmy
def antonym(word, twoLevel=True):
    global datamuse_api
    global anto_cache
    global synanto_cache


    if (anto_cache.empty):
        anto_cache= pd.read_csv("anto_cache.csv", index_col=0,keep_default_na=False)
    if (synanto_cache.empty):
            synanto_cache= pd.read_csv("synanto_cache.csv", index_col=0,keep_default_na=False)

    if word in anto_cache.index:
        res= anto_cache.loc[word,"antonym"]
        if not res == None and not res=="":
            return res
        else:
            return word
    if word in synanto_cache.index and twoLevel:
        res= synanto_cache.loc[word,"antonym"]
        if not res == None and not res=="":
            return res
        else:
            return word
    
    results = datamuse_api.words(rel_ant=word,max=1)

    if len(results)>0:
        res = results[0]["word"]
        anto_cache.loc[word]=res
    elif twoLevel:
        syn = datamuse_api.words(rel_syn=word,max=1)

        if len(syn)>0:
            synonym = syn[0]["word"]
            res=""
            if synonym in anto_cache.index:
                res= anto_cache.loc[synonym,"antonym"]
                if not res == None and not res=="":                    
                    synanto_cache.loc[word]=res
                else:
                    anto_cache.loc[word]=""
                    res=word
            else:
                results =  datamuse_api.words(rel_ant=synonym,max=1)
                if len(results)>0:
                    res = results[0]["word"]
                    synanto_cache.loc[word]=res
                else:
                    anto_cache.loc[word]=""
                    res=word
        else:
            anto_cache.loc[word]=""
            res=word
    else:
        anto_cache.loc[word]=""
        res=word

    anto_cache.to_csv(path_or_buf = "anto_cache.csv")
    synanto_cache.to_csv(path_or_buf = "synanto_cache.csv")
    return res
  

def cosine(a,b):
    dot = np.dot(a, b)
    norma = np.linalg.norm(a)
    normb = np.linalg.norm(b)
    cos = dot / (norma * normb)
    return cos

def makeAltFiles(posFiles,negFiles):
    #get the modeldef
    #nltk.download('averaged_perceptron_tagger')
    toloop = [(negFiles,"PosifedNeg/"),(posFiles,"NegifedPos/")]
    j=0
    for folder in toloop:
        (folderName,filehead)=folder
        for files in folderName:
            j=j+1
            nf = open(filehead+files[10:],"w+")
            f=open(files,"r")
            doc = f.read()
            doc = simple_preprocess(doc)
            lab=label(doc)
            count =0
            outOf=0
            for i, (word,tag) in enumerate(lab):
                if tag in ["JJ","JJR","JJS","RB","RBJ","RBS"]:
                    outOf = outOf+1
                    anto= antonym(word)
                    if anto !=word:
                        count=count+1
                    doc[i]=anto
            nf.write("DATAAMW "+str(count) +" " +str(outOf) +" "+ str(count/outOf)+" \n")
            for word in doc:
                nf.write(word+"\n")

#Montecarlo mean of cosine amoungst the files:
def montecarloavrg(folders,n,model):
    arr=[]
    pfolders= [(folders[1],folders[0])]
    for folder in pfolders:
        cos=0
        for _ in range(0,n):
            a= random.randint(0,len(folder[0])-1)
            b= random.randint(0,len(folder[1])-1)
            v=[]
            for file in [folder[0][a],folder[1][b]]:
                f=open(file,"r")
                doc = f.read()
                doc = simple_preprocess(doc)
                model.random.seed(0)
                vec =model.infer_vector(doc, steps=model.iter)
                v=v+[vec]
            sim = cosine(v[0],v[1])
            cos=cos+sim
        arr= arr+[cos/n]
    return arr

def montecarlopair(folders,model):
    arr=[]
    pfolders= [(folders[0],folders[1]),(folders[2],folders[3])]
    n=len(folders[0])
    for folder in pfolders:
        cos=0
        changes=0
        minVal= 1.0
        maxVal=0
        nu=0
        for i in range(0,n):
            v=[]
            valid=True
            for file in [folder[0][i],folder[1][i]]:
                f=open(file,"r")
                docl = f.readlines()
                f.seek(0)
                doc= f.read()
                if docl[0][:7] == "DATAAMW":
                    data=docl[0].split(" ")
                    num=float(data[3])
                    valid=True
                    if valid:
                        minVal=num if num<minVal else minVal
                        maxVal=num if num>maxVal else maxVal
                        changes=changes + num
                    else:
                        break
                doc = simple_preprocess(doc)
                model.random.seed(0)
                vec =model.infer_vector(doc, steps=model.iter)
                v=v+[vec]
            if valid:
                nu=nu+1
                sim = cosine(v[0],v[1])
                cos=cos+sim
        arr= arr+[(cos/nu,changes/nu,minVal,maxVal,nu)]
    return arr

modelFile="dpo"
#task2.loadAndSaveDocToVed(modelFile, 111,15,10,4,16)
model = Doc2Vec.load(modelFile)
(posFiles,negFiles,posnegFiles,negposFiles) = readin()
#makeAltFiles(posFiles,negFiles)
#print("antobneg - pos ")
#print(montecarloavrg([posnegFiles,posFiles],5000,model))
#print("antobneg - neg ")
#print(montecarloavrg([posnegFiles,negFiles],5000,model))
#print("antopos - pos ")
#print(montecarloavrg([negposFiles,posFiles],5000,model))
#print("antopos - neg ")
#print(montecarloavrg([negposFiles,negFiles],5000,model))
#print(montecarlopair([negFiles,posnegFiles,posFiles,negposFiles],model))
#[(0.9334747908115387, 0.6104543040598577, 0.25, 0.8275862068965517, 1000), (0.9323446993231773, 0.6159889929620137, 0.4, 0.8918918918918919, 1000)]

def writeRes(fn,av,results):
    f = open(fn,"w+")
    f.write(str(av))
    f.write("\n")
    for a in results:
        f.write(str(a)+"\n")
    f.close()

def readRes(fn):
    f=open(fn,"r")
    lines = f.readlines()
    av= float(lines[0].strip("\n"))
    res=[]
    for r in lines[1:]:
        res=res+[float(r.strip("\n"))]
    return(av,res)


print("bow")
#(bav,br)=svm.finalTest(modelFile,posFiles,negFiles,posFiles,negFiles,10,True)
#writeRes("bow",bav,br)
(bav,br)=readRes("bow")
print(bav)
print("DTV")
#(dtvav,dtvr)=svm.finalTest(modelFile,posFiles,negFiles,posFiles,negFiles,10,False)
#writeRes("DTV",dtvav,dtvr)
(dtvav,dtvr)=readRes("DTV")
print(dtvav)
print("AntiBOW")
#(abav,abr)=svm.finalTest(modelFile,posFiles,negFiles,negposFiles,posnegFiles,10,True)
#writeRes("antiBow",abav,abr)
(abav,abr)=readRes("antiBow")
print(abav)
print("antiDTV")
#(adtvav,adtvr)=svm.finalTest(modelFile,posFiles,negFiles,negposFiles,posnegFiles,10,False)
#writeRes("antiDtv",adtvav,adtvr)
(adtvav,adtvr)=readRes("antiDtv")
print(adtvav)

print("combinedDTV")
#(cdtvav,cdtvr)=svm.CompbinedfinalTest(modelFile,posFiles,negFiles,negposFiles,posnegFiles,10,False)
#writeRes("combinedDtv",cdtvav,cdtvr)
(cdtvav,cdtvr)=readRes("combinedDtv")
print(cdtvav)

print("combinedDTVanto")
#(cadtvav,cadtvr)=svm.CompbinedfinalTest(modelFile,negposFiles,posnegFiles,posFiles,negFiles,10,False)
#writeRes("combinedDtvanto",cadtvav,cadtvr)
(cadtvav,cadtvr)=readRes("combinedDtvanto")
print(cadtvav)

print("combinedbow")
#(cbowav,cbowr)=svm.CompbinedfinalTest(modelFile,posFiles,negFiles,negposFiles,posnegFiles,10,True)
#writeRes("combinedBow",cbowav,cbowr)
(cbowav,cbowr)=readRes("combinedBow")
print(cbowav)

print("combinedbowanto")
#(cabowav,cabowr)=svm.CompbinedfinalTest(modelFile,negposFiles,posnegFiles,posFiles,negFiles,10,True)
#writeRes("combinedBowanto",cabowav,cabowr)
(cabowav,cabowr)=readRes("combinedBowanto")
print(cabowav)


import SigTests
print("bow vs DTV")
p=SigTests.permutationTest(np.array(br),np.array(dtvr),5000)
print(p)
print("bow vs AntiBow")
p=SigTests.permutationTest(np.array(br),np.array(abr),5000)
print(p)
print("DTV vs AntiDTV")
p=SigTests.permutationTest(np.array(dtvr),np.array(adtvr),5000)
print(p)

print("bow vs Cbow")
p=SigTests.permutationTest(np.array(br),np.array(cbowr),5000)
print(p)
print("antiBow vs CBowa")
p=SigTests.permutationTest(np.array(cabowr),np.array(abr),5000)
print(p)
print("DTV vs CDTV")
p=SigTests.permutationTest(np.array(dtvr),np.array(cdtvr),5000)
print(p)
print("ADTV vs CDTVA")
p=SigTests.permutationTest(np.array(cadtvr),np.array(cdtvr),5000)
print(p)