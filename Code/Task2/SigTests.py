import math
import numpy as np
import random

def permutationTest(vecA,vecB,MCV):
    #random.seed(608)
    diff= abs(vecA.mean()- vecB.mean())
    print(vecA.mean())
    print(vecB.mean())
    meanDiff=np.array([])
    MCV = min(MCV,pow(2,len(vecA)))
    print(MCV)
    for _ in range(0,MCV):
        A=vecA.copy()
        B=vecB.copy()
        for i,_ in enumerate(vecA):
            rv= random.randint(0, 1)
            if rv ==1:
                swap = vecA[i]
                A[i] = B[i]
                B[i]=swap
        meanDiff=np.append(meanDiff,abs(A.mean()-B.mean()))
    ss=meanDiff>=diff
    s=ss.astype(int).sum()
    p=(s+1)/(MCV+1)
    return p
            
#A = np.array([1,0,1,0,1,1])
#B= np.array([1,1,1,0,1,0])

#print(permutationTest(A,B,500))

