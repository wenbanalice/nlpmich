import glob
from gensim.models import Word2Vec
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from gensim.utils import simple_preprocess

def readin():
    files=[]
    files = files + glob.glob('../../aclImdb/train/neg/*')
    files = files + glob.glob('../../aclImdb/train/pos/*')
    files = files + glob.glob('../../aclImdb/train/unsup/*')
    files = files+ glob.glob('../../aclImdb/test/neg/*')
    files = files+ glob.glob('../../aclImdb/test/pos/*')
    return files

def loadAndSaveDocToVed(name,vs,wind,neg,min,ep):
    files = readin()
    #build doc to vex
    documents = []
    i=0
    for file in files:
        f= open(file,"r")
        lines=f.readlines()
        f.close()
        for line in lines:
            tokens = simple_preprocess(line)
            tDoc = TaggedDocument(tokens,[i])
            documents.append(tDoc)
            i=i+1
    d2v_model = Doc2Vec(seed=0,dm=0, hs =1,vector_size=vs, hashfxn=hash, window=wind,negative=neg, min_count=min, workers=1, epochs =ep)
    #0.85 d2v_model = Doc2Vec(seed=0,dm=0, hs =1,vector_size=110, window=15,negative=5, min_count=2, workers=8, epochs =6)
    d2v_model.build_vocab(documents)
    d2v_model.train(documents, total_examples=d2v_model.corpus_count, epochs=d2v_model.iter)

    fname = name
    d2v_model.save(fname)


#loadAndSaveDocToVed("test1", 125,15,5,2,6)
#print("done")

