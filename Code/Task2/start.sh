module load py-virtualenv-15.1.0-gcc-5.4.0-xqizfzl python-3.6.2-gcc-5.4.0-me5fsee;
python3 -m venv venvs/demo; source venvs/demo/bin/activate;
pip install --upgrade pip; pip install --upgrade numpy scipy gensim;
cd nlp2/nlpmich/Code/Task2;
python3 svm.py